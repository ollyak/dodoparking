﻿using System;
using System.Collections.Generic;
using Domain;

namespace ParkingConsoleApp
{
    public class ParkingDemo
    {
        public Parking Parking;

        public ParkingDemo(Parking parking)
        {
            Parking = parking;
        }

        public void CreateDemoParking()
        {
            Parking.Places = new List<Place>
            {
                new Place {ID = "S1", Size = "S"},
                new Place {ID = "S2", Size = "S"},
                new Place {ID = "S3", Size = "S"},

                new Place {ID = "M1", Size = "M"},
                new Place {ID = "M2", Size = "M"},
                new Place {ID = "M3", Size = "M"},

                new Place {ID = "L1", Size = "L"},
                new Place {ID = "L2", Size = "L"},
                new Place {ID = "L3", Size = "L"}
            };
        }

        public void RunSimulation()
        {
            var parkingGate = new ParkingService(Parking);
            Console.WriteLine("Строим демо-парковку на 9 мест, по 3 каждого размера.");
            
            
            Console.WriteLine("\nПодъехала большая машина...");
            Console.WriteLine(parkingGate.In(new Vehicle {ID = "A111AA", Size = "L"}));
            
            Console.WriteLine("\nПодъехала большая машина...");
            Console.WriteLine(parkingGate.In(new Vehicle {ID = "В222ВВ", Size = "L"}));
            
            Console.WriteLine("\nПодъехала большая машина...");
            Console.WriteLine(parkingGate.In(new Vehicle {ID = "С333СС", Size = "L"}));
            
            Console.WriteLine("\nПодъехала большая машина. Мест должно не хватить...");
            Console.WriteLine(parkingGate.In(new Vehicle {ID = "D444DD", Size = "L"}));
            
            Console.WriteLine("\nПодъехала средняя машина...");
            Console.WriteLine(parkingGate.In(new Vehicle {ID = "E555EE", Size = "M"}));
            
            Console.WriteLine("\nУехала большая машина с места L2...");
            Console.WriteLine(parkingGate.Out(new Vehicle {ID = "В222ВВ", Size = "L"}));
            
            Console.WriteLine("\nТа большая машина приехала на парковку еще раз. Теперь ей найдется место...");
            Console.WriteLine(parkingGate.In(new Vehicle {ID = "D444DD", Size = "L"}));

        }
    }
}