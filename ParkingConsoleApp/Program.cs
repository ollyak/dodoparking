﻿using System;
using Domain;

namespace ParkingConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Запусаем демо-парковку");
            
            var demo = new ParkingDemo(new Parking());
            demo.CreateDemoParking();
            demo.RunSimulation();
            
            Console.WriteLine("\nРабота демо-парковки завершена.");
        }
    }
}