﻿namespace Domain
{
    public class Place
    {
        public string ID { get; set; }
        public string Size { get; set; }
        public bool Busy { get; set; }
        public string VehicleID { get; set; }

    }
}