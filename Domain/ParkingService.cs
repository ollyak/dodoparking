﻿using System;

namespace Domain
{
    public class ParkingService : IParking
    {
        public Parking Parking;
        
        public ParkingService(Parking parking)
        {
            Parking = parking;
        }

        public string In(Vehicle vehicle)
        {
            var placeFound = FindFreePlaceBySize(vehicle.Size);
            
            if (placeFound == null)
            {
                // TODO show message to vehicle driver
                return "Все места уже заняты, извините. Ближайшая парковка за углом.";
            }

            placeFound.Busy = true;
            placeFound.VehicleID = vehicle.ID;
            // TODO print Talon to vehicle driver
            return "Паркуйтесь на место " + placeFound.ID;
        }

        public string Out(Vehicle vehicle)
        {
            // set place free
            var placeFound = FindPlaceByVehicleID(vehicle.ID);
            if (placeFound == null)
            {
                throw new Exception("У нас пробелма. Автомобиль не найден на парковке.");
            }
            placeFound.Busy = false;
            placeFound.VehicleID = null;
            return "Счастливого пути!";
        }

        private Place FindFreePlaceBySize(string size)
        {
            return Parking.Places.Find(i => i.Size == size && i.Busy != true);
        }

        private Place FindPlaceByVehicleID(string vehicleID)
        {
            return Parking.Places.Find(i => i.VehicleID == vehicleID);
        }
    }
}