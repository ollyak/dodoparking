﻿namespace Domain
{
    public class Talon
    {
        public Place Place { get; set; }
        public Vehicle Vehicle { get; set; }
    }
}