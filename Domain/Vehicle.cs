﻿using System;

namespace Domain
{
    public class Vehicle
    {
        public string ID { get; set; }
        public string Size { get; set; }
    }
}