﻿namespace Domain
{
    public interface IParking
    {
        string In(Vehicle vehicle);
        string Out(Vehicle vehicle);
    }
}